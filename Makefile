start:
	symfony server:start

generate:
	rm -rf prod_folder
	mkdir prod_folder
	mkdir prod_folder/var
	mkdir prod_folder/var/cache
	mkdir prod_folder/var/cache/prod
	cp -R assets prod_folder/assets
	cp -R config prod_folder/config
	cp -R htdocs prod_folder/htdocs
	cp -R public prod_folder/public
	cp -R src prod_folder/src
	cp -R templates prod_folder/templates
	cp -R composer.json prod_folder/composer.json

