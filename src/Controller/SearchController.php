<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use App\Client\DatacenterClient;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'search')]
    public function search(Request $request, DatacenterClient $datacenterClient): Response
    {
        $search = $request->get('search');
        $data['title'] = 'Search "' . $search . '"';
        $searchWithUnderscore = str_replace(" ", "_", $search);

        if ($search) {
            $data['books'] = $datacenterClient->getSearchBooks($search);
            $data['films'] = $datacenterClient->getSearchFilms($search);
            $data['games'] = $datacenterClient->getSearchGames($search);
            $data['music'] = $datacenterClient->getSearchMusic($search);
            $data['profiles'] = $datacenterClient->getSearchProfiles($search);
            $data['series'] = $datacenterClient->getSearchSeries($search);
            $data['sports'] = $datacenterClient->getSearchSports($search);
            $data['types'] = $datacenterClient->getSearchTypes($searchWithUnderscore);
        }

        return $this->render('search.html.twig', $data);
    }
}