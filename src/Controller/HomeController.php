<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use App\Client\DatacenterClient;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(DatacenterClient $datacenterClient): Response
    {
        $data = [];
        foreach($datacenterClient->getTypes() as $type) {
            $data[$type->family][] = $type;
        }
        
        return $this->render('home.html.twig', [
            'data' => $data,
        ]);
    }
}