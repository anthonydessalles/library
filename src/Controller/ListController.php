<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use App\Client\DatacenterClient;

class ListController extends AbstractController
{

    #[Route('/books', name: 'books')]
    public function books(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'books',
            'data' => $datacenterClient->getBooks(),
        ]);
    }

    #[Route('/films', name: 'films')]
    public function films(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'films',
            'data' => $datacenterClient->getFilms(),
        ]);
    }

    #[Route('/gameplay', name: 'gameplay')]
    public function gameplay(DatacenterClient $datacenterClient): Response
    {
        return $this->render('gameplay.html.twig', [
            'current_page_name' => 'gameplay',
            'data' => $datacenterClient->getPlaylists(),
        ]);
    }

    #[Route('/games', name: 'games')]
    public function games(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'games',
            'data' => $datacenterClient->getGames(),
        ]);
    }

    #[Route('/music', name: 'music')]
    public function music(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'music',
            'data' => $datacenterClient->getMusic(),
        ]);
    }

    #[Route('/profiles', name: 'profiles')]
    public function profiles(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'profiles',
            'data' => $datacenterClient->getProfiles(),
        ]);
    }

    #[Route('/recordings', name: 'recordings')]
    public function recordings(DatacenterClient $datacenterClient): Response
    {
        return $this->render('recordings.html.twig', [
            'current_page_name' => 'recordings',
            'data' => $datacenterClient->getRecordings(),
        ]);
    }

    #[Route('/series', name: 'series')]
    public function series(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'series',
            'data' => $datacenterClient->getSeries(),
        ]);
    }

    #[Route('/sports', name: 'sports')]
    public function sports(DatacenterClient $datacenterClient): Response
    {
        return $this->render('list.html.twig', [
            'current_page_name' => 'sports',
            'data' => $datacenterClient->getSports(),
        ]);
    }
}