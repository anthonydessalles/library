<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use App\Client\DatacenterClient;

class PageController extends AbstractController
{
    #[Route('/books/{id}', name: 'books_page')]
    public function book(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'books',
            'data' => $datacenterClient->getBook($id),
        ]);
    }

    #[Route('/films/{id}', name: 'films_page')]
    public function films(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'films',
            'data' => $datacenterClient->getFilm($id),
        ]);
    }

    #[Route('/games/{id}', name: 'games_page')]
    public function game(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'games',
            'data' => $datacenterClient->getGame($id),
        ]);
    }

    #[Route('/music/{id}', name: 'music_page')]
    public function music(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'music',
            'data' => $datacenterClient->getMusicById($id),
        ]);
    }

    #[Route('/profiles/{id}', name: 'profiles_page')]
    public function profiles(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'profiles',
            'data' => $datacenterClient->getProfile($id),
        ]);
    }

    #[Route('/series/{id}', name: 'series_page')]
    public function series(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'series',
            'data' => $datacenterClient->getSerie($id),
        ]);
    }

    #[Route('/sports/{id}', name: 'sports_page')]
    public function sports(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'sports',
            'data' => $datacenterClient->getSport($id),
        ]);
    }

    #[Route('/types/{id}', name: 'types_page')]
    public function type(int $id, DatacenterClient $datacenterClient): Response
    {
        return $this->render('page.html.twig', [
            'current_page_name' => 'types',
            'data' => $datacenterClient->getType($id),
            'books' => $datacenterClient->getBooksByType($id),
            'films' => $datacenterClient->getFilmsByType($id),
            'games' => $datacenterClient->getGamesByType($id),
            'music' => $datacenterClient->getMusicByType($id),
            'profiles' => $datacenterClient->getProfilesByType($id),
            'series' => $datacenterClient->getSeriesByType($id),
            'sports' => $datacenterClient->getSportsByType($id),
        ]);
    }

    #[Route('/yearly', name: 'yearly')]
    public function yearly(DatacenterClient $datacenterClient): Response
    {
        $years = [];

        foreach($datacenterClient->getYearlyFilms() as $film) {
            $film->type = 'films';
            $film->name = $film->title;
            $years[$film->ofTheYear][] = $film;
        }

        foreach($datacenterClient->getYearlyGames() as $game) {
            $game->type = 'games';
            $years[$game->ofTheYear][] = $game;
        }

        foreach($datacenterClient->getYearlyProfiles() as $profile) {
            $profile->type = 'profiles';
            $years[$profile->ofTheYear][] = $profile;
        }
        
        krsort($years);

        return $this->render('yearly.html.twig', [
            'years' => $years,
        ]);
    }

    #[Route('/yearly/{year}', name: 'yearly_year')]
    public function yearlyYear(int $year, DatacenterClient $datacenterClient): Response
    {
        $data['title'] = $year;
        $universesTypeId = [];

        foreach($datacenterClient->getTypes() as $type) {
            if('universes' == $type->family) {
                $universesTypeId[] = $type->id;
            }
        }

        $data['films'] = $datacenterClient->getUniversesFilmsByYear($year, $universesTypeId);
        $data['games'] = $datacenterClient->getUniversesGamesByYear($year, $universesTypeId);
        $data['series'] = $datacenterClient->getUniversesSeriesByYear($year, $universesTypeId);

        return $this->render('search.html.twig', $data);
    }
}