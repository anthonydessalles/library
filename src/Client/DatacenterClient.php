<?php

namespace App\Client;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class DatacenterClient
{
    private Client $client;
    
    public function __construct(string $endpoint, string $username, string $password)
    {
        $client = new Client();
        $response = $client->post($endpoint . 'login_check', [
            RequestOptions::JSON => [
                "username" => $username,
                "password" => $password
            ]
        ]);
        $token = json_decode($response->getBody(), true)['token'];

        $this->client = new Client([
            'base_uri' => $endpoint,
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ]
        ]);
    }

    public function checkGoogleToken(string $token)
    {
        return json_decode($this->client->request('POST', 'tokensignin', [
            'content_type' => 'application/x-www-form-urlencoded',
            'form_params' => [
                'idtoken' => $token,
            ]
        ])->getBody());
    }

    public function getBook(int $id)
    {
        return json_decode($this->client->request('GET', 'books/' . $id)->getBody());
    }

    public function getBooks()
    {
        return json_decode($this->client->request('GET', 'books?groups[]=list')->getBody());
    }

    public function getBooksByType(int $id)
    {
        return json_decode($this->client->request('GET', 'books?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getFilm(int $id)
    {
        return json_decode($this->client->request('GET', 'films/' . $id)->getBody());
    }

    public function getFilms()
    {
        return json_decode($this->client->request('GET', 'films?groups[]=list')->getBody());
    }

    public function getFilmsByType(int $id)
    {
        return json_decode($this->client->request('GET', 'films?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getGame(int $id)
    {
        return json_decode($this->client->request('GET', 'games/' . $id)->getBody());
    }

    public function getGames()
    {
        return json_decode($this->client->request('GET', 'games?groups[]=list')->getBody());
    }

    public function getGamesByType(int $id)
    {
        return json_decode($this->client->request('GET', 'games?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getMusic()
    {
        return json_decode($this->client->request('GET', 'music?groups[]=list')->getBody());
    }

    public function getMusicById(int $id)
    {
        return json_decode($this->client->request('GET', 'music/' . $id)->getBody());
    }

    public function getMusicByType(int $id)
    {
        return json_decode($this->client->request('GET', 'music?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getPlaylists()
    {
        return json_decode($this->client->request('GET', 'playlists?groups[]=list&status=public')->getBody());
    }

    public function getProfile(int $id)
    {
        return json_decode($this->client->request('GET', 'profiles/' . $id)->getBody());
    }

    public function getProfiles()
    {
        return json_decode($this->client->request('GET', 'profiles?groups[]=list')->getBody());
    }

    public function getProfilesByType(int $id)
    {
        return json_decode($this->client->request('GET', 'profiles?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getRecordings()
    {
        return json_decode($this->client->request('GET', 'recordings')->getBody());
    }

    public function getSearchBooks(string $title)
    {
        return json_decode($this->client->request('GET', 'books/?groups[]=list&title=' . $title)->getBody());
    }

    public function getSearchFilms(string $title)
    {
        return json_decode($this->client->request('GET', 'films/?groups[]=list&title=' . $title)->getBody());
    }

    public function getSearchGames(string $title)
    {
        return json_decode($this->client->request('GET', 'games/?groups[]=list&name=' . $title)->getBody());
    }

    public function getSearchMusic(string $title)
    {
        return json_decode($this->client->request('GET', 'music/?groups[]=list&title=' . $title)->getBody());
    }

    public function getSearchProfiles(string $title)
    {
        return json_decode($this->client->request('GET', 'profiles/?groups[]=list&name=' . $title)->getBody());
    }

    public function getSearchSeries(string $title)
    {
        return json_decode($this->client->request('GET', 'series/?groups[]=list&title=' . $title)->getBody());
    }

    public function getSearchSports(string $title)
    {
        return json_decode($this->client->request('GET', 'sports/?groups[]=list&title=' . $title)->getBody());
    }

    public function getSearchTypes(string $title)
    {
        return json_decode($this->client->request('GET', 'types/?groups[]=list&slug=' . $title)->getBody());
    }

    public function getSerie(int $id)
    {
        return json_decode($this->client->request('GET', 'series/' . $id)->getBody());
    }

    public function getSeries()
    {
        return json_decode($this->client->request('GET', 'series?groups[]=list')->getBody());
    }

    public function getSeriesByType(int $id)
    {
        return json_decode($this->client->request('GET', 'series?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getSport(int $id)
    {
        return json_decode($this->client->request('GET', 'sports/' . $id)->getBody());
    }

    public function getSports()
    {
        return json_decode($this->client->request('GET', 'sports?groups[]=list')->getBody());
    }

    public function getSportsByType(int $id)
    {
        return json_decode($this->client->request('GET', 'sports?groups[]=list&types.id=' . $id)->getBody());
    }

    public function getType(int $id)
    {
        return json_decode($this->client->request('GET', 'types/' . $id)->getBody());
    }

    public function getTypes()
    {
        return json_decode($this->client->request('GET', 'types?exists[family]=1')->getBody());
    }

    public function getUniversesFilmsByYear(int $year, array $ids)
    {
        return json_decode($this->client->request('GET', 'films?groups[]=list&year=' . $year . '&types.id[]=' . (implode("&types.id[]=", $ids)))->getBody());
    }

    public function getUniversesGamesByYear(int $year, array $ids)
    {
        return json_decode($this->client->request('GET', 'games?groups[]=list&date=' . $year . '&types.id[]=' . (implode("&types.id[]=", $ids)))->getBody());
    }

    public function getUniversesSeriesByYear(int $year, array $ids)
    {
        return json_decode($this->client->request('GET', 'series?groups[]=list&year=' . $year . '&types.id[]=' . (implode("&types.id[]=", $ids)))->getBody());
    }

    public function getYearlyFilms()
    {
        return json_decode($this->client->request('GET', 'films?groups[]=list&exists[ofTheYear]=1')->getBody());
    }

    public function getYearlyGames()
    {
        return json_decode($this->client->request('GET', 'games?groups[]=list&exists[ofTheYear]=1')->getBody());
    }

    public function getYearlyProfiles()
    {
        return json_decode($this->client->request('GET', 'profiles?groups[]=list&exists[ofTheYear]=1')->getBody());
    }
}
