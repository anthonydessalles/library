<?php

namespace App\Security;

use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use App\Client\DatacenterClient;

final class OauthAuthenticator extends OAuth2Authenticator implements AuthenticationEntryPointInterface
{
    public function __construct(
        private readonly OAuth2ClientInterface $oauthClient,
        private readonly AuthenticationSuccessHandlerInterface $authenticationSuccessHandler,
        private readonly DatacenterClient $datacenterClient
    ) {
    }

    public function supports(Request $request): ?bool
    {
        return 'login' === $request->attributes->get('_route');
    }

    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return $this->oauthClient->redirect([], []);
    }

    public function authenticate(Request $request): Passport
    {
        $accessToken = $this->fetchAccessToken($this->oauthClient);
        $resourceOwner = $this->oauthClient->fetchUserFromToken($accessToken);

        if (null == $this->datacenterClient->checkGoogleToken($accessToken->getValues()['id_token'])) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            throw new \Exception('User not found in DB');
        }

        return new SelfValidatingPassport(new UserBadge($resourceOwner->getId()));
    }
 
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return $this->authenticationSuccessHandler->onAuthenticationSuccess($request, $token);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {    
        return $this->start($request, $exception);
    }
}